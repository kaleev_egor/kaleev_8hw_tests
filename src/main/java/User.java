import java.util.Map;

public class User {
    private int userId;
    private int pin;
    private Map<Integer, Integer> bankAccount;

    public User(int userId, int pin, Map<Integer, Integer> bankAccount) {
        this.userId = userId;
        this.pin = pin;
        this.bankAccount = bankAccount;
    }

    //public int getUserId() {
    //    return userId;
    //}

    public Map<Integer, Integer> getBankAccount() {
        return bankAccount;
    }

    public int getPin() {
        return pin;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", pin=" + pin +
                ", bankAccount=" + bankAccount +
                '}';
    }
}
