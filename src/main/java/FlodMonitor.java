import MyExceptions.FlodMonitorException;

public class FlodMonitor {
    public FlodMonitor() {
    }

    public void checkTransfer(int cash) throws FlodMonitorException {
        if (cash > 10000) {
            throw new FlodMonitorException("Свяжитесь с оператором для подтверждения перевода более 10000 тубриков");
        }
    }
}
