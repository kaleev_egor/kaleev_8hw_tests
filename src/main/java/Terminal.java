import MyExceptions.AccountlsLockedException;
import MyExceptions.FlodMonitorException;
import MyExceptions.TranslationException;

public class Terminal implements ITerminal {
    private final TerminalServer server;
    private final PinValidator pinValidator;
    private final FlodMonitor flodMonitor;

    private User user = null;
    private Integer accountNumber = null;
    private boolean logged = false;

    public Terminal(TerminalServer server, PinValidator pinValidator, FlodMonitor flodMonitor) {
        this.server = server;
        this.pinValidator = pinValidator;
        this.flodMonitor = flodMonitor;
    }

    @Override
    public void enterCardNumber(int accountNumber) {
        user = server.checkAccountExists(accountNumber);
        if (user != null) {
            this.accountNumber = accountNumber;
            System.out.println("Карта в терминале, получен номер счета");
        } else {
            System.out.println("Неверный номер счета/карта недействительна");
        }
    }

    @Override
    public void enterPin(int pin) {
        if (!logged) {

            try {
                pinValidator.enterPin(server, user, pin);
                System.out.println("Пароль успешно введен");
                logged = true;
            } catch (AccountlsLockedException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("Вы уже вошли");
        }

    }

    @Override
    public void doTransfer(int recipientCardNumber, int cash) {
        if (!logged) {
            System.out.println("Операция перевода невозможна до входа в системму");
        } else if (cash % 100 != 0) {
            System.out.println("Перевод возможен только суммами, кратными 100");
        } else {
            try {
                flodMonitor.checkTransfer(cash);
                server.doTransfer(recipientCardNumber, user, accountNumber, cash);
            } catch (FlodMonitorException e) {
                System.out.println(e.getMessage());
            } catch (TranslationException e) {
                System.out.println(e.getMessage());
            }
        }

    }

    @Override
    public void exit() {
        if (accountNumber != null) {
            System.out.println("Карта извлечена из терминала");
            accountNumber = null;
            user = null;
            logged = false;
        }
    }
}
