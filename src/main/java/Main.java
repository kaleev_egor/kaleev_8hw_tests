import MyExceptions.AccountlsLockedException;


public class Main {
    public static void main(String[] args) throws AccountlsLockedException, InterruptedException {


        ITerminal terminal = new Terminal(new TerminalServer(), new PinValidator(), new FlodMonitor());

        // вставили карту
        terminal.enterCardNumber(1001);


        //вводим пароль
        terminal.enterPin(1212);
        terminal.enterPin(1212);
        terminal.enterPin(1212);
        terminal.enterPin(1212);
        terminal.enterPin(1212);
        Thread.sleep(6000);

        terminal.enterPin(1111);

        // переводим деньги

        terminal.doTransfer(1003, 9000);
        terminal.doTransfer(1000, 900);
        terminal.doTransfer(100, 100);// неправильный номер счета

        // забираем карту
        terminal.exit();
    }
}
