package MyExceptions;

public class AccountlsLockedException extends Exception{
    public AccountlsLockedException(String message){
        super(message);
    }
}
