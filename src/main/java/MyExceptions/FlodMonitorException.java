package MyExceptions;

public class FlodMonitorException extends Exception{
    public FlodMonitorException(String message){
        super(message);
    }
}
