import MyExceptions.AccountlsLockedException;

import java.text.SimpleDateFormat;

public class PinValidator {
    private int countIncorrectPin;
    private long unlockTime;

    public PinValidator() {
        this.countIncorrectPin = 0;
    }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");

    public void enterPin(TerminalServer server, User user, int pin) throws AccountlsLockedException {
        if(unlockTime < System.currentTimeMillis()){
            if(!server.checkPin(user, pin)){
                countIncorrectPin++;
                if(countIncorrectPin>2){
                    unlockTime = System.currentTimeMillis()+5000;
                    countIncorrectPin = 0;
                    throw new AccountlsLockedException("Неверный пароль, аккаунт заблокирован на 5 секнд");
                }
                throw new AccountlsLockedException("Неверный пароль");
            }
        }
        else{
            throw new AccountlsLockedException("Доступ к аккаунту временно ограничен, время разблокировки: "+ sdf.format(unlockTime) + " текущее время: "+ sdf.format(System.currentTimeMillis()));
        }
    }
}
