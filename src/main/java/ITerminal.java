import MyExceptions.AccountlsLockedException;

public interface ITerminal {
    void enterCardNumber(int cardNumber);

    void enterPin(int pin) throws AccountlsLockedException;

    void doTransfer(int recipientId, int cash);

    void exit();
}
