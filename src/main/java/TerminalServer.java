
import MyExceptions.TranslationException;

import java.util.HashMap;
import java.util.Map;

public class TerminalServer {
    Map<Integer, User> mapOfUsers = new HashMap<>();

    public TerminalServer() {
        Map<Integer, Integer> bankAccounts1 = new HashMap<>();
        bankAccounts1.put(1000, 500_00);
        bankAccounts1.put(1001, 100_00);

        Map<Integer, Integer> bankAccounts2 = new HashMap<>();
        bankAccounts2.put(1003, 200_00);

        mapOfUsers.put(1, new User(1, 1111, bankAccounts1));
        mapOfUsers.put(2, new User(2, 2222, bankAccounts2));
    }

    public User checkAccountExists(int accountNumber) {
        for (User user : mapOfUsers.values()) {
            if (user.getBankAccount().keySet().contains(accountNumber)) {
                return user;
            }
        }
        return null;
    }

    public boolean checkPin(User user, int pin) {
        if (user.getPin() == pin) {
            return true;
        }
        return false;
    }

    public void doTransfer(int recipientCardNumber, User sender, int accountNumber, int cash) throws TranslationException {

        if (sender.getBankAccount().get(accountNumber) < cash) {
            throw new TranslationException("Недостаточно средств для осуществления перевода");
        }
        User recipient = null;
        for (User user : mapOfUsers.values()) {
            if (user.getBankAccount().keySet().contains(recipientCardNumber)) {
                recipient = user;
                break;
            }
        }
        if(recipient == null){
            throw new TranslationException("Недопустимый счет получателя");
        }
        try {
            System.out.println(sender);
            System.out.println(recipient);
            sender.getBankAccount().put(accountNumber, sender.getBankAccount().get(accountNumber) - cash);
            recipient.getBankAccount().put(recipientCardNumber, recipient.getBankAccount().get(recipientCardNumber) + cash);
            System.out.println(sender);
            System.out.println(recipient);
        } catch (Exception e) {
            throw new TranslationException("Перевод не осуществлен, ошибка сети");
        }
    }
}
